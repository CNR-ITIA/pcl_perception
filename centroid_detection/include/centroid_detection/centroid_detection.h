#ifndef centroid_dectection_20190605
#define centroid_dectection_20190605

#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/MarkerArray.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/features/moment_of_inertia_estimation.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/sac_model_sphere.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/kdtree/kdtree_flann.h>
//#include <pcl/octree/octree_search.h>
//#include <pcl/octree/octree.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/octree/octree_pointcloud.h>
#include <pcl/surface/mls.h>
#include <tf/transform_listener.h>
#include <Eigen/Geometry>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseArray.h>
#include <robot_self_filter/self_see_filter.h>
#include <centroid_detection/dynparConfig.h>
#include <dynamic_reconfigure/server.h>
#include <eigen_conversions/eigen_msg.h>


#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/PlanningScene.h>

namespace cnr_perception
{

class CentroidDetection
{
protected:

  ros::NodeHandle nh;
  ros::NodeHandle pnh;
  ros::Publisher pc_grid_pub;
  ros::Publisher pc_without_plane_pub;
  ros::Publisher pc_pub;
  ros::Publisher array_pub;
  ros::Publisher centroid_pub;
  ros::Publisher vis_array_pub;
  ros::Subscriber sub;
  moveit_msgs::PlanningScene planning_scene;
  ros::Publisher planning_scene_diff_publisher;


  double confidence_lb=6e-4;
  double confidence_ub=1e10;

  double intensity_lb=2e-3;
  double intensity_ub=1e8;

  double x_lb=-5;
  double x_ub=5;

  double y_lb=-5;
  double y_ub=5;

  double z_lb=0;
  double z_ub=2.2;

  double angle_x_lb=-M_PI;
  double angle_x_ub= M_PI;

  double angle_y_lb=-M_PI;
  double angle_y_ub= M_PI;

  double angle_xy_lb=-M_PI;
  double angle_xy_ub= M_PI;

  int cluster_min_size=300;
  int cluster_max_size=3000;

  double voxel_dim=0.05;
  double octree_grid_resolution=0.05;
  bool pub_pointcloud=false;

  bool m_publish_obstacle=false;
  bool m_publish_cube_obstacles=false;
  int m_last_cube_obstacles=0;
  int m_last_obb_obstacles=0;
  double m_cube_ostacle_offset=0.1;

  std::vector<std::string> m_touch_links;
  Eigen::Affine3d T_reference_camera;
  std::string reference_frame;
  std::string camera_frame;

  tf::TransformBroadcaster br;
  tf::TransformListener    listener;

  std::shared_ptr<filters::SelfFilter<pcl::PointXYZ>> self_filter;
  std::vector<std::string> frames;

  dynamic_reconfigure::Server<centroid_detection::dynparConfig> dynsrv;

public:
  CentroidDetection(const ros::NodeHandle& node_handle, const ros::NodeHandle& private_node_handle, const bool subscribe_pc=true);

  void cloudCallback (const sensor_msgs::PointCloud2ConstPtr& cloud_msg);

  void parameterCallback(centroid_detection::dynparConfig& new_config, uint32_t level);

};

}


#endif
