#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PoseArray.h>
#include <subscription_notifier/subscription_notifier.h>
#include <tf/transform_listener.h>
#include <Eigen/Geometry>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <rosdyn_core/primitives.h>
#include <std_msgs/Int64.h>
#include <subscription_notifier/subscription_notifier.h>
#include <sensor_msgs/PointCloud2.h>

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "centroid_merge");
  ros::NodeHandle nh;
  ros::Rate lp(12.5);

  ros_helper::SubscriptionNotifier<sensor_msgs::PointCloud2> sub(nh,"input",1);

  if (!sub.waitForANewData(ros::Duration(100)))
  {
    ROS_ERROR("no input point cloud");
    return  0;
  }

  while (ros::ok())
  {

  }

  return 0;
}

