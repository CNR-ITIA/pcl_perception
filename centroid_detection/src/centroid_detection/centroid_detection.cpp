#include <centroid_detection/centroid_detection.h>

namespace cnr_perception
{

CentroidDetection::CentroidDetection(const ros::NodeHandle& node_handle,
                                     const ros::NodeHandle& private_node_handle,
                                     const bool subscribe_pc):
  nh(node_handle),
  pnh(private_node_handle)
{
  tf::TransformListener listener;

  if (!nh.getParam("reference_frame",reference_frame))
  {
    ROS_ERROR("No reference_frame speficied");
    throw std::invalid_argument("No reference_frame speficied");
  }
  if (!pnh.getParam("camera_frame",camera_frame))
  {
    ROS_ERROR("No camera_frame speficied");
    throw std::invalid_argument("No camera_frame speficied");
  }
  if (!pnh.getParam("publish_obstacles",m_publish_obstacle))
  {
    ROS_ERROR("publish_obstacles no set, use false");
    m_publish_obstacle=false;
  }

  m_publish_cube_obstacles=pnh.param("publish_cube_obstacles",false);
  m_cube_ostacle_offset=pnh.param("cube_ostacle_offset",0.1);
  if (!pnh.getParam("touch_links",m_touch_links))
  {
    m_touch_links.clear();
  }

  bool tf_received=false;


  tf::StampedTransform camera_to_reference;
  for (unsigned int idx=0;idx<100;idx++)
  {
    ros::spinOnce();
    try
    {
      listener.lookupTransform(reference_frame,camera_frame,ros::Time(0),camera_to_reference);
      tf_received=true;
      ROS_INFO("transformation from %s to %s is ok",camera_frame.c_str(),reference_frame.c_str());
      break;
    }
    catch(...)
    {
      ROS_INFO("no transformation from %s to %s, wait 1 second",camera_frame.c_str(),reference_frame.c_str());
      ros::Duration(1).sleep();
    }
  }

  if (!tf_received)
  {
    ROS_ERROR("no transformation from %s to %s",camera_frame.c_str(),reference_frame.c_str());
    throw std::invalid_argument("No transformation from camera frame to reference frame");
  }

  tf::transformTFToEigen(camera_to_reference,T_reference_camera);


  self_filter.reset(new filters::SelfFilter<pcl::PointXYZ>(pnh));

  self_filter->getSelfMask()->getLinkNames(frames);
  for (const std::string& s: frames)
    ROS_INFO("frame=%s",s.c_str());


  // Create a ROS subscriber for the input point cloud
  if (subscribe_pc)
    sub = nh.subscribe<sensor_msgs::PointCloud2> ("input", 1, &CentroidDetection::cloudCallback, this);

  array_pub=pnh.advertise<geometry_msgs::PoseArray> ("poses",1);
  centroid_pub=pnh.advertise<geometry_msgs::PoseArray> ("centroids",1);
  vis_array_pub=pnh.advertise<visualization_msgs::MarkerArray> ("vis_poses",1);
  pc_pub=pnh.advertise<sensor_msgs::PointCloud2> ("filter",1);
  pc_grid_pub=pnh.advertise<sensor_msgs::PointCloud2> ("grid",1);
  pc_without_plane_pub=pnh.advertise<sensor_msgs::PointCloud2> ("no_plane",1);
  planning_scene_diff_publisher = nh.advertise<moveit_msgs::PlanningScene>("planning_scene", 1);
  dynamic_reconfigure::Server<centroid_detection::dynparConfig>::CallbackType f = boost::bind(&CentroidDetection::parameterCallback, this, _1, _2);
  dynsrv.setCallback(f);
}

void CentroidDetection::cloudCallback (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
  // Container for original & filtered data
  boost::shared_ptr<pcl::PCLPointCloud2> cloud(new pcl::PCLPointCloud2);

  // Convert to PCL data type
  pcl_conversions::toPCL(*cloud_msg, *cloud);

  std::vector<std::string> fields;
  for (const pcl::PCLPointField& f: cloud->fields)
  {
    fields.push_back(f.name);
  }
  pcl::PassThrough<pcl::PCLPointCloud2> passthrough_filter;
  pcl::PCLPointCloud2Ptr filt_pc(new pcl::PCLPointCloud2());

  // confidence filter
  if (std::find(fields.begin(),fields.end(),"confidence")!=fields.end())
  {
    passthrough_filter.setFilterFieldName("confidence");
    passthrough_filter.setFilterLimits(confidence_lb,confidence_ub);
    passthrough_filter.setInputCloud(cloud);
    passthrough_filter.filter(*filt_pc);
  }
  else
  {
    *filt_pc=*cloud;
  }

  // intensity filter
  if (std::find(fields.begin(),fields.end(),"intensity")!=fields.end())
  {
    passthrough_filter.setFilterFieldName("intensity");
    passthrough_filter.setFilterLimits(intensity_lb,intensity_ub);
    passthrough_filter.setInputCloud(filt_pc);
    passthrough_filter.filter(*filt_pc);
  }
  // x,y,z filters
  passthrough_filter.setFilterFieldName("x");
  passthrough_filter.setFilterLimits(x_lb,x_ub);
  passthrough_filter.setInputCloud(filt_pc);
  passthrough_filter.filter(*filt_pc);

  passthrough_filter.setFilterFieldName("y");
  passthrough_filter.setFilterLimits(y_lb,y_ub);
  passthrough_filter.setInputCloud(filt_pc);
  passthrough_filter.filter(*filt_pc);

  passthrough_filter.setFilterFieldName("z");
  passthrough_filter.setFilterLimits(z_lb,z_ub);
  passthrough_filter.setInputCloud(filt_pc);
  passthrough_filter.filter(*filt_pc);

  // filter angle
  if (std::find(fields.begin(),fields.end(),"angle_x")!=fields.end())
  {
    passthrough_filter.setFilterFieldName("angle_x");
    passthrough_filter.setFilterLimits(angle_x_lb,angle_x_ub);
    passthrough_filter.setInputCloud(filt_pc);
    passthrough_filter.filter(*filt_pc);
  }
  if (std::find(fields.begin(),fields.end(),"angle_y")!=fields.end())
  {

    passthrough_filter.setFilterFieldName("angle_y");
    passthrough_filter.setFilterLimits(angle_y_lb,angle_y_ub);
    passthrough_filter.setInputCloud(filt_pc);
    passthrough_filter.filter(*filt_pc);
  }
  if (std::find(fields.begin(),fields.end(),"angle_xy")!=fields.end())
  {

    passthrough_filter.setFilterFieldName("angle_xy");
    passthrough_filter.setFilterLimits(angle_xy_lb,angle_xy_ub);
    passthrough_filter.setInputCloud(filt_pc);
    passthrough_filter.filter(*filt_pc);
  }
  // Perform the actual filtering
  pcl::PCLPointCloud2 voxel_pc;
  pcl::VoxelGrid<pcl::PCLPointCloud2> voxelizer;
  voxelizer.setInputCloud (filt_pc);
  voxelizer.setLeafSize (voxel_dim, voxel_dim, voxel_dim);
  voxelizer.filter (voxel_pc);



  sensor_msgs::PointCloud2 output;
  output.header.stamp=ros::Time::now();
  output.header.frame_id=camera_frame;

  // Create the segmentation object for the planar model and set all the parameters
  pcl::PointCloud<pcl::PointXYZ>::Ptr voxel_points (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromPCLPointCloud2( voxel_pc, *voxel_points);





  pcl_conversions::moveFromPCL(voxel_pc, output);
  output.header.stamp=ros::Time::now();
  output.header.frame_id=camera_frame;


  // Publish the data
  if (pub_pointcloud)
    pc_grid_pub.publish (output);


  pcl::PointCloud<pcl::PointXYZ>::Ptr sf_cloud(new pcl::PointCloud<pcl::PointXYZ>());

  self_filter->updateWithSensorFrame(*voxel_points, *sf_cloud, camera_frame);

  sensor_msgs::PointCloud2 sf_cloud_msg;

  pcl::toROSMsg(*sf_cloud, sf_cloud_msg);

  sf_cloud_msg.header.stamp=ros::Time::now();

  sf_cloud_msg.header.frame_id=camera_frame;
  pc_pub.publish(sf_cloud_msg);


  geometry_msgs::PoseArray centroids;
  centroids.header.frame_id=reference_frame;
  centroids.header.stamp=ros::Time::now();
  geometry_msgs::PoseArray pose_array;
  pose_array.header=centroids.header;

  visualization_msgs::MarkerArray markers;
  visualization_msgs::Marker marker;
  marker.header=centroids.header;
  marker.type=visualization_msgs::Marker::CUBE;
  marker.id = 0;
  marker.action = visualization_msgs::Marker::DELETEALL;
  markers.markers.push_back(marker);
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = 1;
  marker.pose.position.y = 1;
  marker.pose.position.z = 1;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = octree_grid_resolution;
  marker.scale.y = octree_grid_resolution;
  marker.scale.z = octree_grid_resolution;
  marker.color.a = 1.0; // Don't forget to set the alpha!
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;


  visualization_msgs::Marker box_marker;
  box_marker.header.frame_id=camera_frame;
  box_marker.header.stamp=ros::Time::now();
  box_marker.id = 1000;
  box_marker.type=visualization_msgs::Marker::CUBE;
  box_marker.action = visualization_msgs::Marker::ADD;
  box_marker.pose.position.x = 1;
  box_marker.pose.position.y = 1;
  box_marker.pose.position.z = 1;
  box_marker.pose.orientation.x = 0.0;
  box_marker.pose.orientation.y = 0.0;
  box_marker.pose.orientation.z = 0.0;
  box_marker.pose.orientation.w = 1.0;
  box_marker.scale.x = octree_grid_resolution;
  box_marker.scale.y = octree_grid_resolution;
  box_marker.scale.z = octree_grid_resolution;
  box_marker.color.a = 0.5; // Don't forget to set the alpha!
  box_marker.color.r = 0.0;
  box_marker.color.g = 0.0;
  box_marker.color.b = 1.0;

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  if (sf_cloud->size()==0)
  {
    array_pub.publish(pose_array);
    centroid_pub.publish(centroids);
    vis_array_pub.publish(markers);
    return;
  }
  tree->setInputCloud (sf_cloud);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  ec.setClusterTolerance (2*voxel_dim);
  ec.setMinClusterSize (cluster_min_size);
  ec.setMaxClusterSize (cluster_max_size);
  ec.setSearchMethod (tree);
  ec.setInputCloud (sf_cloud);
  ec.extract (cluster_indices);



  planning_scene.world.collision_objects.clear();

  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
  {
    double red=marker.color.r;
    marker.color.r = marker.color.g;
    marker.color.g = red;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f (new pcl::PointCloud<pcl::PointXYZ>);

    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
      cloud_cluster->points.push_back (sf_cloud->points[*pit]); //*
    cloud_cluster->width = cloud_cluster->points.size ();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;

    pcl::MomentOfInertiaEstimation<pcl::PointXYZ> feature_extractor;
    feature_extractor.setInputCloud (cloud_cluster);
    feature_extractor.compute ();

    std::vector <float> moment_of_inertia;
    std::vector <float> eccentricity;
    pcl::PointXYZ min_point_AABB;
    pcl::PointXYZ max_point_AABB;
    pcl::PointXYZ min_point_OBB;
    pcl::PointXYZ max_point_OBB;
    pcl::PointXYZ position_OBB;
    Eigen::Matrix3f rotational_matrix_OBB;
    float major_value, middle_value, minor_value;
    Eigen::Vector3f major_vector, middle_vector, minor_vector;
    Eigen::Vector3f mass_center;

    feature_extractor.getMomentOfInertia (moment_of_inertia);
    feature_extractor.getEccentricity (eccentricity);
    feature_extractor.getAABB (min_point_AABB, max_point_AABB);
    feature_extractor.getOBB (min_point_OBB, max_point_OBB, position_OBB, rotational_matrix_OBB);
    feature_extractor.getEigenValues (major_value, middle_value, minor_value);
    feature_extractor.getEigenVectors (major_vector, middle_vector, minor_vector);
    feature_extractor.getMassCenter (mass_center);

    Eigen::Affine3d T_cam_OBB;
    T_cam_OBB.setIdentity();


    T_cam_OBB.translation()(0)=position_OBB.x;
    T_cam_OBB.translation()(1)=position_OBB.y;
    T_cam_OBB.translation()(2)=position_OBB.z;
    T_cam_OBB.linear()=rotational_matrix_OBB.cast<double>();
    Eigen::Affine3d T_ref_OBB=T_reference_camera*T_cam_OBB;

    tf::poseEigenToMsg(T_ref_OBB,box_marker.pose);
    box_marker.pose.orientation.x=0;
    box_marker.pose.orientation.y=0;
    box_marker.pose.orientation.z=0;
    box_marker.pose.orientation.w=1;

    box_marker.scale.x=max_point_OBB.x-min_point_OBB.x;
    box_marker.scale.y=max_point_OBB.y-min_point_OBB.y;
    box_marker.scale.z=max_point_OBB.z-min_point_OBB.z;
    markers.markers.push_back(box_marker);


    moveit_msgs::AttachedCollisionObject oriented_bounding_box_obstacle;
    oriented_bounding_box_obstacle.link_name = reference_frame;
    oriented_bounding_box_obstacle.object.header.frame_id = reference_frame;
    oriented_bounding_box_obstacle.object.id = camera_frame+"_box"+std::to_string(box_marker.id);
    oriented_bounding_box_obstacle.touch_links=m_touch_links;

    /* Define a box to be attached */
    shape_msgs::SolidPrimitive primitive;
    primitive.type = primitive.BOX;
    primitive.dimensions.resize(3);
    primitive.dimensions[0] = 0.5+m_cube_ostacle_offset; //box_marker.scale.x+m_cube_ostacle_offset;
    primitive.dimensions[1] = 0.5+m_cube_ostacle_offset; //box_marker.scale.y+m_cube_ostacle_offset;
    primitive.dimensions[2] = 1.2+m_cube_ostacle_offset; //box_marker.scale.z+m_cube_ostacle_offset;

    oriented_bounding_box_obstacle.object.primitives.push_back(primitive);
    oriented_bounding_box_obstacle.object.primitive_poses.push_back(box_marker.pose);
    box_marker.id++;


    pcl::octree::OctreePointCloud<pcl::PointXYZ> octree (octree_grid_resolution);

    octree.setInputCloud (cloud_cluster);
    octree.addPointsFromInputCloud();

    std::vector<pcl::PointXYZ, Eigen::aligned_allocator<pcl::PointXYZ> > points;
    octree.getOccupiedVoxelCenters(points);

    geometry_msgs::Pose pose;
    pose.orientation.w=1;
    for (const pcl::PointXYZ& p: points)
    {

      Eigen::Vector3d p_in_camera;
      p_in_camera(0)=p.x;
      p_in_camera(1)=p.y;
      p_in_camera(2)=p.z;

      Eigen::Vector3d p_in_ref=T_reference_camera*p_in_camera;
      pose.position.x=p_in_ref(0);
      pose.position.y=p_in_ref(1);
      pose.position.z=p_in_ref(2);
      pose_array.poses.push_back(pose);


      if (m_publish_cube_obstacles)
      {
        moveit_msgs::AttachedCollisionObject cube_obstacles;
        cube_obstacles.link_name = reference_frame;
        cube_obstacles.object.header.frame_id = reference_frame;
        cube_obstacles.object.id = camera_frame+"_box"+std::to_string(marker.id);
        cube_obstacles.touch_links=m_touch_links;

        /* Define a box to be attached */
        shape_msgs::SolidPrimitive primitive;
        primitive.type = primitive.BOX;
        primitive.dimensions.resize(3);
        primitive.dimensions[0] = octree_grid_resolution+m_cube_ostacle_offset;
        primitive.dimensions[1] = octree_grid_resolution+m_cube_ostacle_offset;
        primitive.dimensions[2] = octree_grid_resolution+m_cube_ostacle_offset;

        cube_obstacles.object.primitives.push_back(primitive);
        cube_obstacles.object.primitive_poses.push_back(pose);
        planning_scene.world.collision_objects.push_back(cube_obstacles.object);

        moveit_msgs::ObjectColor col;
        col.color.r=0;
        col.color.g=1;
        col.color.b=0;
        col.color.a=0.10;
        col.id=cube_obstacles.object.id;
        planning_scene.object_colors.push_back(col);

      }
      marker.pose=pose;
      marker.id++;
      markers.markers.push_back(marker);

    }




    if (m_publish_obstacle)
      planning_scene.world.collision_objects.push_back(oriented_bounding_box_obstacle.object);


    Eigen::Affine3d centroid_in_reference=T_reference_camera*T_cam_OBB;
    tf::poseEigenToMsg(centroid_in_reference,pose);
    centroids.poses.push_back(pose);

  }

  if (m_publish_cube_obstacles)
  {
    for (int im=marker.id;im<m_last_cube_obstacles;im++)
    {
      moveit_msgs::CollisionObject cube_obstacles;
      cube_obstacles.header.frame_id = reference_frame;
      cube_obstacles.id = camera_frame+"_box"+std::to_string(im);
      cube_obstacles.operation=1; // remove
      planning_scene.world.collision_objects.push_back(cube_obstacles);
    }
    m_last_cube_obstacles=marker.id;
  }

  if (m_publish_obstacle)
  {
    for (int im=box_marker.id;im<m_last_obb_obstacles;im++)
    {
      moveit_msgs::CollisionObject cube_obstacles;
      cube_obstacles.header.frame_id = reference_frame;
      cube_obstacles.id = camera_frame+"_box"+std::to_string(im);
      cube_obstacles.operation=1; // remove
      planning_scene.world.collision_objects.push_back(cube_obstacles);
    }
    m_last_obb_obstacles=box_marker.id;
  }


  if (m_publish_obstacle || m_publish_cube_obstacles)
  {

    planning_scene.is_diff = true;
    planning_scene_diff_publisher.publish(planning_scene);
  }

  array_pub.publish(pose_array);
  centroid_pub.publish(centroids);
  vis_array_pub.publish(markers);
}

void CentroidDetection::parameterCallback(centroid_detection::dynparConfig &new_config, uint32_t level)
{
  x_lb=new_config.x_lb;
  x_ub=new_config.x_ub;

  y_lb=new_config.y_lb;
  y_ub=new_config.y_ub;

  z_lb=new_config.z_lb;
  z_ub=new_config.z_ub;

  angle_x_lb=new_config.angle_x_lb;
  angle_x_ub=new_config.angle_x_ub;

  angle_y_lb=new_config.angle_y_lb;
  angle_y_ub=new_config.angle_y_ub;

  angle_xy_lb=new_config.angle_xy_lb;
  angle_xy_ub=new_config.angle_xy_ub;

  confidence_lb=new_config.confidence_lb;
  confidence_ub=new_config.confidence_ub;

  intensity_lb=new_config.intensity_lb;
  intensity_ub=new_config.intensity_ub;

  cluster_min_size=new_config.cluster_min_size;
  cluster_max_size=new_config.cluster_max_size;
  voxel_dim=new_config.voxel_grid_resolution;
  octree_grid_resolution=new_config.octree_grid_resolution;
}
}
