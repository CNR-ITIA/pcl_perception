#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/MarkerArray.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
#include <nav_msgs/OccupancyGrid.h>


ros::Publisher grid_pub;

double confidence_lb=6e4;
double confidence_ub=1e6;

double intensity_lb=2e3;
double intensity_ub=1e8;

double min_distance=0.5; //w.r.t. the camera
double max_distance=2.0; 
int cluster_min_size=300;
bool pub_pointcloud=false;
void 
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
  // Container for original & filtered data
  boost::shared_ptr<pcl::PCLPointCloud2> cloud(new pcl::PCLPointCloud2); 
  pcl::PointCloud<pcl::PointXYZ>::Ptr plane_filtered_cloud(new pcl::PointCloud<pcl::PointXYZ>); 
  
  
  pcl::PCLPointCloud2 intensity_pc;
  
  
  // Convert to PCL data type
  pcl_conversions::toPCL(*cloud_msg, *cloud);
  
  
  pcl::PassThrough<pcl::PCLPointCloud2> passthrough_filter;
  passthrough_filter.setFilterFieldName("confidence");
  passthrough_filter.setFilterLimits(confidence_lb,confidence_ub);
  passthrough_filter.setInputCloud(cloud);
  
  pcl::PCLPointCloud2Ptr PFcloudPtr(new pcl::PCLPointCloud2());
  passthrough_filter.filter(*PFcloudPtr);
  
  passthrough_filter.setFilterFieldName("intensity");
  passthrough_filter.setFilterLimits(intensity_lb,intensity_ub);
  passthrough_filter.setInputCloud(PFcloudPtr);
  pcl::PCLPointCloud2Ptr PFcloudPtr2(new pcl::PCLPointCloud2());
  passthrough_filter.filter(*PFcloudPtr2);
  
  
  double dx=0.1;
  double xmin=-2;
  double xmax=2;
  unsigned int xsize=std::ceil((xmax-xmin)/dx);
  
  double dy=dx;
  double ymin=-2;
  double ymax=2;
  unsigned int ysize=std::ceil((ymax-ymin)/dy);
  
  nav_msgs::OccupancyGrid grid;
  grid.header=cloud_msg->header;
  grid.info.resolution=(xmax-xmin)/xsize;
  grid.info.height=xsize;
  grid.info.width=ysize;
//   grid.info.origin.position.x=(xmax+xmin)*0.5;
//   grid.info.origin.position.y=(ymax+ymin)*0.5;
  grid.info.origin.position.x=xmin;
  grid.info.origin.position.y=ymin;
  grid.info.origin.position.z=max_distance;
  grid.info.origin.orientation.w=1;
  
  grid.data.resize(xsize*ysize,0);
  
  // Perform the actual filtering
  pcl::PCLPointCloud2 voxel_pc;
  pcl::VoxelGrid<pcl::PCLPointCloud2> voxelizer;
  voxelizer.setInputCloud (PFcloudPtr2);
  voxelizer.setLeafSize (dx*0.1, dy*0.1, 0.01);
  voxelizer.filter (voxel_pc);
  
   // Create the segmentation object for the planar model and set all the parameters
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
  
  pcl::fromPCLPointCloud2( voxel_pc, *cloud_filtered);
  
  
  for( const pcl::PointXYZ& p: cloud_filtered->points)
  {
//     ROS_INFO("x=%f,y=%f,z=%f",p.x,p.y,p.z);
    unsigned int ix=floor((p.x-xmin)/dx);
    unsigned int iy=floor((p.y-ymin)/dx);
    if ((p.z>min_distance) && (p.z<max_distance))
    {
//       grid.data.at(ix*xsize+iy)=100;
      grid.data.at(ix+iy*ysize)+=2.0;
      if (grid.data.at(ix+iy*ysize)>100.0)
        grid.data.at(ix+iy*ysize)=100;
    }
    
  }
  grid_pub.publish(grid);
}

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "centroid_detection");
  ros::NodeHandle nh;
  
  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe<sensor_msgs::PointCloud2> ("input", 1, cloud_cb);
  grid_pub = nh.advertise<nav_msgs::OccupancyGrid>("grid",1);
  
  // Spin
  while (ros::ok())
  {
    ros::spinOnce();
    nh.getParam("confidence_ub",confidence_ub);
    nh.getParam("confidence_lb",confidence_lb);
    nh.getParam("intensity_ub",intensity_ub);
    nh.getParam("intensity_lb",intensity_lb);
    nh.getParam("min_distance",min_distance);
    nh.getParam("max_distance",max_distance);
    ros::Duration(1e-2).sleep();

  }
}
