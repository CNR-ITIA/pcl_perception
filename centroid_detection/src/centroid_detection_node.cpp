#include <centroid_detection/centroid_detection.h>

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "centroid_detection");
  ros::NodeHandle nh;
  ros::NodeHandle pnh("~");

  cnr_perception::CentroidDetection cd(nh,pnh);

  ros::spin();
}
