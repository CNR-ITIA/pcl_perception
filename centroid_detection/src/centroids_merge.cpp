#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PoseArray.h>
#include <subscription_notifier/subscription_notifier.h>
#include <tf/transform_listener.h>
#include <Eigen/Geometry>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>
#include <rosdyn_core/primitives.h>
#include <std_msgs/Int64.h>

int main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "centroid_merge");
  ros::NodeHandle nh;
  ros::Rate lp(12.5);
  ros::Publisher vis_array_pub=nh.advertise<visualization_msgs::MarkerArray> ("vis_poses",1);
  ros::Publisher array_pub=nh.advertise<geometry_msgs::PoseArray> ("poses",1);
  ros::Publisher centroid_pub=nh.advertise<geometry_msgs::PoseArray> ("centroids",1);

  tf::TransformListener listener;

  std::vector<std::string> tof_cameras;
  if (!nh.getParam("tof_cameras",tof_cameras))
  {
    ROS_ERROR("No centroids speficied");
    return -1;
  }

  std::string reference_frame;
  if (!nh.getParam("reference_frame",reference_frame))
  {
    ROS_ERROR("No reference_frame speficied");
    throw std::invalid_argument("No reference_frame speficied");
  }

  std::vector<std::shared_ptr<ros_helper::SubscriptionNotifier<visualization_msgs::MarkerArray>>> vis_centroid_recs;
  std::vector<std::shared_ptr<ros_helper::SubscriptionNotifier<geometry_msgs::PoseArray>>> poses_recs;
  std::vector<std::shared_ptr<ros_helper::SubscriptionNotifier<geometry_msgs::PoseArray>>> centroids_merge_recs;

  for (const std::string& centroid: tof_cameras)
  {
    std::shared_ptr<ros_helper::SubscriptionNotifier<visualization_msgs::MarkerArray>> vis_ptr(new ros_helper::SubscriptionNotifier<visualization_msgs::MarkerArray>(nh,centroid+"/vis_poses",1));
    vis_centroid_recs.push_back(vis_ptr);
    std::shared_ptr<ros_helper::SubscriptionNotifier<geometry_msgs::PoseArray>> ptr(new ros_helper::SubscriptionNotifier<geometry_msgs::PoseArray>(nh,centroid+"/poses",1));
    poses_recs.push_back(ptr);
    std::shared_ptr<ros_helper::SubscriptionNotifier<geometry_msgs::PoseArray>> centroid_ptr(new ros_helper::SubscriptionNotifier<geometry_msgs::PoseArray>(nh,centroid+"/centroids",1));
    centroids_merge_recs.push_back(centroid_ptr);
  }

  while (ros::ok())
  {
    ros::spinOnce();
    visualization_msgs::MarkerArray vis_array;
    visualization_msgs::Marker marker;
    marker.type=visualization_msgs::Marker::CUBE;
    marker.id = 0;
    marker.action = visualization_msgs::Marker::DELETEALL;
    vis_array.markers.push_back(marker);

    geometry_msgs::PoseArray poses;
    geometry_msgs::PoseArray centroids;

    for (std::size_t idx=0;idx<tof_cameras.size();idx++)
    {
      visualization_msgs::MarkerArray cam1_vis= vis_centroid_recs.at(idx)->getData();
      for (std::size_t ip=1;ip<cam1_vis.markers.size();ip++)
      {
        vis_array.markers.push_back(cam1_vis.markers.at(ip));
        vis_array.markers.back().id=++marker.id;

      }
      if (not poses_recs.at(idx)->isANewDataAvailable())
        continue;

      geometry_msgs::PoseArray cam_array=poses_recs.at(idx)->getData();

      Eigen::Affine3d T_base_camera;
      tf::StampedTransform tf_base_camera;
      T_base_camera.setIdentity();
      bool error=false;
      if (cam_array.header.frame_id.compare(reference_frame))
      {
        if (not listener.waitForTransform(reference_frame.c_str(),cam_array.header.frame_id,cam_array.header.stamp,ros::Duration(0.01)))
        {
          error=true;
        }
        else
        {
          listener.lookupTransform(reference_frame,cam_array.header.frame_id,cam_array.header.stamp,tf_base_camera);
          tf::poseTFToEigen(tf_base_camera,T_base_camera);
        }
      }
      else
      {
        tf::poseEigenToTF(T_base_camera,tf_base_camera);
      }


      for (const geometry_msgs::Pose& p: cam_array.poses)
      {
        Eigen::Vector3d point_in_c;
        point_in_c(0)=p.position.x;
        point_in_c(1)=p.position.y;
        point_in_c(2)=p.position.z;
        Eigen::Vector3d point_in_r=T_base_camera*point_in_c;
        geometry_msgs::Pose p_in_r;
        p_in_r.position.x=point_in_r(0);
        p_in_r.position.y=point_in_r(1);
        p_in_r.position.z=point_in_r(2);
        poses.poses.push_back(p_in_r);
      }
      poses.header.frame_id=reference_frame;
      poses.header.stamp=ros::Time::now();

      geometry_msgs::PoseArray centroids_array=centroids_merge_recs.at(idx)->getData();
      for (const geometry_msgs::Pose& p: centroids_array.poses)
      {
        Eigen::Vector3d point_in_c;
        point_in_c(0)=p.position.x;
        point_in_c(1)=p.position.y;
        point_in_c(2)=p.position.z;
        Eigen::Vector3d point_in_r=T_base_camera*point_in_c;
        geometry_msgs::Pose p_in_r;
        p_in_r.position.x=point_in_r(0);
        p_in_r.position.y=point_in_r(1);
        p_in_r.position.z=point_in_r(2);
        centroids.poses.push_back(p_in_r);
      }
      centroids.header.frame_id=reference_frame;
      centroids.header.stamp=ros::Time::now();
    }

    poses.header.stamp=ros::Time::now();
    poses.header.frame_id=reference_frame;

    centroids.header.stamp=ros::Time::now();
    centroids.header.frame_id=reference_frame;

    vis_array_pub.publish(vis_array);
    array_pub.publish(poses);
    centroid_pub.publish(centroids);
    lp.sleep();
  }

  return 0;
}
